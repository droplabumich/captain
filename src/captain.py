#!/usr/bin/env python

from __future__ import division

import math
import numpy as np

from sensor_msgs.msg import Imu
from vehicle_interface.srv import  LoadController
from std_msgs.msg import Header
from utils.msg import StampedBool, PressureSensor

import rospy
import roslib
import tf

from utils import utils


class Captain():
    def __init__(self, name):

        self.name = name
        self.get_config()

        self.sub_data = rospy.Subscriber('/sphere_a/nav_sensors/imu', Imu, self.getAttitude, queue_size=10)
        self.sub_depth =  rospy.Subscriber('/sphere_a/nav_sensors/pressure_sensor', PressureSensor, self.getDepth, queue_size=10)
        self.camera_enable_pub = rospy.Publisher('/sphere_hw_iface/enable_camera_trigger', StampedBool, queue_size=10)
        self.camera_enabled = False

        self.r = 0
        self.p = 0
        self.y = 0

        self.state = "disarmed"
        self.state_machine_period = 0.1

        # Mission parameters
        #self.mission_time = 600
        self.mission_started = False
        self.mission_stopped = False
        self.pitch_threshold = self.pitch_threshold * (math.pi/180)
        self.pitch_threshold_counter = 0
        #self.pitch_threshold_arm_time = 30
        #self.pitch_threshold_release_time = 3

        rospy.loginfo("Pitch threshold is: "+str(self.pitch_threshold))
        
        # Mission abort parameters
        self.roll_threshold = self.roll_threshold  * (math.pi/180)       # degrees
        #self.roll_threshold_time = 3    # seconds
        self.roll_threshold_counter = 0
        self.roll_threshold_check_period = 0.1
        self.depth_counter = 0

        rospy.wait_for_service("/pilot/ctrl_full")
        self.controller_srv = rospy.ServiceProxy("/pilot/ctrl_full", LoadController)

        # Timers
        rospy.Timer(rospy.Duration(self.state_machine_period), self.statemachine)
        rospy.Timer(rospy.Duration(self.roll_threshold_check_period), self.checkforabort)
        rospy.Timer(rospy.Duration(1), self.publishcamerastate)

    def publishcamerastate(self, dummy):
        msg = StampedBool()
        msg.header.stamp = rospy.Time.now()
        msg.enable = self.camera_enabled

        self.camera_enable_pub.publish(msg)

    def getAttitude(self, msg):
        (self.r, self.p, self.y) = tf.transformations.euler_from_quaternion([msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w])

    def getDepth(self, msg):
        if msg.depth > self.max_depth:
            self.depth_counter += 1
        else: 
            self.depth_counter = 0 

        if self.depth_counter > 50: 
            self.state = "abort"

    def statemachine(self, dummy):
#        rospy.loginfo(str(self.r)+" "+ str(self.p)+" "+str(self.y))
 #       rospy.loginfo("Pitch threshold counter "+str(self.pitch_threshold_counter))
        if self.state == "disarmed":
            #checkAngleBiggerCouter(self.p, self.pitch_threshold, self.pitch_threshold_counter)

            if abs(self.p) > self.pitch_threshold:
                self.pitch_threshold_counter += 1
                
            else: 
                self.pitch_threshold_counter = 0

            #print(self.pitch_threshold_counter)
            if self.pitch_threshold_counter > (self.pitch_threshold_arm_time/self.state_machine_period):
                self.state = "armed"
                self.pitch_threshold_counter = 0
                rospy.loginfo("Captain: The vehicle is now armed")
        elif self.state == "armed":
            #checkAngleSmallerCouter(self.p, self.pitch_threshold, self.pitch_threshold_counter)
            
            if abs(self.p) < self.pitch_threshold:
                self.pitch_threshold_counter += 1   
            else: 
                self.pitch_threshold_counter = 0

            if self.pitch_threshold_counter >  (self.pitch_threshold_release_time/self.state_machine_period):
                self.state = "released"
                self.pitch_threshold_counter = 0
                rospy.loginfo("Captain: The vehicle is now released")
        elif self.state == "released":
            if not self.mission_started:
                # Call Start mission service 
                if self.start_mission():
                    rospy.loginfo("Captain: The vehicle has started its mission")
                    self.stop_timer = rospy.Timer(rospy.Duration(self.mission_time), self.stop_mission_timer_callback, oneshot=True)
                    self.mission_started = True
                else:
                    rospy.loginfo("Captain: The vehicle has failed to start its mission. Retrying...")

        elif self.state == "stopped":
            if not self.mission_stopped:
                if self.stop_mission():
                    rospy.loginfo("Captain: The vehicle has stopped its mission")       
                    self.mission_stopped = True

        elif self.state == "abort":
            if not self.mission_stopped:
                if self.stop_mission():
                    rospy.loginfo("Captain: The vehicle has aborted its mission")
                    self.mission_stopped = True


    def checkforabort(self, dummy):

#        checkAngleBiggerCouter(self.r, self.roll_threshold, self.roll_threshold_counter)
        if abs(self.r) > self.roll_threshold:
            self.roll_threshold_counter += 1
                
        else: 
            self.roll_threshold_counter = 0
        
        if self.roll_threshold_counter > (self.roll_threshold_time/self.roll_threshold_check_period) :
            self.state = "abort"
            self.roll_threshold_counter = 0

    def stop_mission_timer_callback(self, dummy):
        rospy.loginfo("Captain: Mission time elapsed")       
        self.state = "stopped"

    def start_mission(self):
        try: 
            header = Header()
            self.controller_srv(header, "start", [])    # Start the controller mission 
            self.camera_enabled = True                  # Start the camera and strobes

            return True
        except rospy.ServiceException,  e:
            print("Service call failed: %s"%e) 
            return False
        

    def stop_mission(self):
        try:
            header = Header()
            self.controller_srv(header,"stop", [])      # Stop the controller mission 
            self.camera_enabled = False                 # Stop the camera and strobes
            return True
        except rospy.ServiceException, e:
            print("Service call failed: %s"%e)
    
    def get_config(self):
        """ Get config from param server """

        param_dict = {'mission_time':        "/mission_time",
                      'pitch_threshold':       "/pitch_threshold",
                      'pitch_threshold_arm_time': "/pitch_threshold_arm_time",
                      'pitch_threshold_release_time': "/pitch_threshold_release_time",
                      'roll_threshold': "/roll_threshold", 
                      'max_depth': "/max_depth", 
                      'roll_threshold_time': "roll_threshold_time"}



        if not utils.getRosParams(self, param_dict, self.name):
            rospy.logfatal("%s: shutdown due to invalid config parameters!", self.name)
            exit(0)  # TODO: find a better way

def checkAngleBiggerCouter(angle, threshold, counter):
    if abs(angle) > threshold:
        counter += 1
        print("Hello")
    else: 
        counter = 0

def checkAngleSmallerCouter(angle, threshold, counter):
    if abs(angle) < threshold:
        counter += 1
    else: 
        counter = 0


def main():
    rospy.init_node('Captain')
    name = rospy.get_name()
    rospy.loginfo('%s initializing ...', name)

    watch = Captain(name)
    rospy.spin()

if __name__ == '__main__':
    main()
